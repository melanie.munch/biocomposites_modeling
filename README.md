# Biocomposite Modeling

## Abstract

This paper presents a workflow for the design of transformation processes using different kinds of expert’s knowledge. It introduces POND (Process and observation ONtology Discovery), a workflow dedicated to answer expert’s questions about processes. It addresses two main issues: (1) how to represent the processes inner complexity, and (2) how to reason about processes taking into account uncertainty and causality. First, we show how to use a semantic model, an ontology, and its associated data to answer some of the expert’s questions concerning the processes, using semantic web languages and technologies. Then, we describe how to learn a predictive model, to discover new knowledge and provide explicative models by integrating the semantic model into a probabilistic relational model. The result is a complete workflow able to extensively analyze transformation processes through all their granularity levels and answer expert’s questions about their domains. An example of this workflow is given on biocomposites manufacturing for food packaging.

## Note

Reverse engineering is not included entirely for now since the database used for querying is not public.

## Related Article

Mélanie Munch, Patrice Buche, Stéphane Dervaux, Juliette Dibie, Liliana Ibanescu, Cristina Manfredotti, Pierre-Henri Wuillemin, Hélène Angellier-Coussy,
Combining ontology and probabilistic models for the design of bio-based product transformation processes,
Expert Systems with Applications,
Volume 203,
2022,
117406,
ISSN 0957-4174,
https://doi.org/10.1016/j.eswa.2022.117406.